import numbers
class Calculate:
	def is_number(self, x):
		return isinstance(x, numbers.Number)

	def add(self, x, y):
		if type(x) == int and type(y) == int:
			return x + y
		else:
			raise TypeError("Invalid type: {} and {}".format(type(x), type(y)))

	def sub(self, x, y):
		if type(x) == int and type(y) == int:
			return x - y
		else:
			raise TypeError("Invalid type: {} and {}".format(type(x), type(y)))

	def mul(self, x, y):
		if type(x) == int and type(y) == int:
			return x * y
		else:
			raise TypeError("Invalid type: {} and {}".format(type(x), type(y)))

	def div(self, x, y):
		if type(x) == int and type(y) == int:
			return x / y
		else:
			raise TypeError("Invalid type: {} and {}".format(type(x), type(y)))

if __name__ == '__main__':
	calc = Calculate()
	result = calc.add(2, 2)
	result2 = calc.sub(2, 2)
	result3 = calc.mul(2, 2)
	result4 = calc.div(2, 2)
	print(result)
	print(result2)
	print(result3)
	print(result4)

#start the debugger here
#import pdb
#pdb.set_trace()
