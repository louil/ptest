import unittest
from calculate import Calculate

class TestCalculate(unittest.TestCase):
	def setUp(self):
		self.calc = Calculate()

	def test_add_ret_correctly(self):
		self.assertEqual(4, self.calc.add(2,2))
	
	def test_add_strings_raise_eror(self):
		self.assertRaises(TypeError, self.calc.add, 'a', 'b')

	def test_sub_strings_raise_eror(self):
		self.assertEqual(4, self.calc.sub(2,2))

	def test_sub_strings_raise_eror(self):
		self.assertRaises(TypeError, self.calc.sub, 'a', 'b')

	def test_mul_strings_raise_eror(self):
		self.assertEqual(4, self.calc.mul(2,2))

	def test_mul_strings_raise_eror(self):
		self.assertRaises(TypeError, self.calc.mul, 'a', 'b')

	def test_div_strings_raise_eror(self):
		self.assertEqual(0, self.calc.div(2,2))

	def test_div_strings_raise_eror(self):
		self.assertRaises(TypeError, self.calc.div, 'a', 'b')

if __name__ == '__main__':
	unittest.main()
